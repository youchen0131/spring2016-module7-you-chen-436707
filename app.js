'use strict';

angular.module('coffees', ['ui.bootstrap']);

// Declare app level module which depends on views, and components
var coffees = angular.module('coffees', [
  'ngRoute',
  'coffeesControllers'
]);


coffees.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
	when('/coffees', {
	 templateUrl:'coffees.html',
	 controller:'CoffeesCtrl'
	}).
	when('/coffees/:coffeeId', {
	 templateUrl:'reviews.html',
	 controller:'CoffeesCtrl'
	})
	.otherwise({redirectTo: '/coffees'});
}]);
